import { useEffect, useState } from "react";

export const useFeath = (url: string) => {

    const [data, setData] = useState(null);
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        setLoading(true);
        fetch(url)
            .then((response) => response.json())
            .then((data) => setData(data))
            .finally(() => setLoading(false));
    }, []);

    return { data, loading };
}
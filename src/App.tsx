import {useFeath} from "./use.featch";
import "./App.css";

function App() {
  const {data, loading} = useFeath("http://localhost:3000/api/users");
  return (
    <div className="App">
      <h1>Get All List Users</h1>
      <div className="ListuUsers">
        <ul>
          {loading && <li>Loading...</li>}
          {data?.map((user: any) => (
            <li key={user.id}>
              {user.name} {user.lastName}{" "}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}

export default App;
